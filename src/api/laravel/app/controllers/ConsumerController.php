<?php
/**
 * Created by PhpStorm.
 * User: lgalaz
 * Date: 5/27/14
 * Time: 6:00 PM
 */

class ConsumerController extends BaseController {

    public function get(){
        $consumers = Consumer::all();

        return Response::json(array(
                'error' => false,
                'consumers' => $consumers->toArray()),
            200
        );
    }
}