<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



Route::pattern('id', '[1-9][0-9]*');
//will only accept dates in YYYY-mm-dd format
Route::pattern('date', '^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$');

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('api/consumers', [
    "as"   => "consumers/get",
    "uses" => "ConsumerController@get"
]);

Route::get('api/consumers/{id}',[
    "as"   => "consumers/byId",
    "uses" => "ConsumerController@byId"
]);

Route::post('api/consumers', [
    "as" => "consumers/post",
    "uses" => "ConsumerController@post"
]);
