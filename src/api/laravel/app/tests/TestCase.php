<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

    private static $isSetup = false;

    public function setUp()
    {
        parent::setUp(); // Don't forget this!

        if(!self::$isSetup)
        {
            $this->prepareForTests();
            self::$isSetup = true;
        }
    }

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../bootstrap/start.php';
	}

    private function prepareForTests()
    {
        Artisan::call('migrate');
/*
        $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
        foreach ($tableNames as $name) {
            //if you don't want to truncate migrations
            if ($name == 'migrations') {
                continue;
            }
            DB::table($name)->truncate();
        }

        Artisan::call('db:seed');
        Mail::pretend(true);*/
    }


}
