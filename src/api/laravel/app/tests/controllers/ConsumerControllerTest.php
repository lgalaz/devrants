<?php

//sample run    phpunit --filter testGet_ReturnsOkAndNoErrors
class ConsumerControllerTest extends TestCase {

    function __construct() {
        parent::__construct();

    }

    private $exampleConsumer = 'name';

    public function testGet_ReturnsOkAndNoErrors()
    {
        $response = $this->action('GET', 'ConsumerController@get',[],[],[],[]);

        $array = json_decode($response->getContent());

        $this->assertFalse($array->error);
        $this->assertEquals(200, $response->getStatusCode());
    }


} 