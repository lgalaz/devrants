Random stupid instructions for forgetful bastards.
==================================================

</br>

Installing in dev machine
------------------
</br>
Install composer: 
	- curl -sS https://getcomposer.org/installer | php
	- mv composer.phar /usr/local/bin/composer

Run composer install at the root of the project:
	- composer install

Create a virtual host in apache to serve the api.

</br>

Use vagrant box to run the project
	- install vagrant 
	- clone adyktos devbox from "https://github.com/adykto/devBox"
	- go to the folder where the vagrant file is. and run "vagrant up"

Give write permission to the app/storage folder.


Getting XDebug to work remotely with phpstorm
---------------------------------------------

Configuring PHPStorm

Create Project
</br>
Add Remote Server
</br>
	-File -> Settings
	-Under Project Settings [project-name] on the left, browse to PHP -> Servers
	-Click the green +
	-Check “Use path mappings (select if the server is remote or symlinks are used)”
	-Under File/Directory on the left, Browse to Project Files -> \home\xyz\hz...
	-Next to the www directory, on the right under Absolute path on the server, enter: /CrackedMonkeys/src (or wherever your files are stored on the vagrant box)
	-Click OK
</br>
Add Debug Config
</br>
	-Under the Run dropdown menu, click Edit Configurations…
	-Click PHP Web Application, then click the green +
	-Name: vagrant
	-Server: remote.crackedmonkeys.com should be in the dropdown
	-Start URL: /
	-Browser: Chrome
</br>
Enable xdebug with php
	-sudo apt-get install php5-xdebug
	-add settings to xdebug.ini
		xdebug.remote_enable=1
		xdebug.remote_handler=dbgp
		xdebug.remote_port=9000
		xdebug.remote_log= "/tmp/log/xdebug.log"
		xdebug.remote_host=10.11.12.1


Urls
----

DESCRIPTION         | URL               | VERB  | SAMPLE RAW BODY/JSON
get all products    | api/products      | GET
get one product     | api/products/{id} | GET
add one product     | api/productS      | POST  | { "name" : "example", "description" :  "example", "price" :  3.5 }



