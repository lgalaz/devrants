<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function($table)
        {
            $table->increments("id");
            $table->string("username")
                ->unique()
                ->nullable()
                ->default(null);
            $table->string("name")
                ->nullable()
                ->default(null);
            $table->string("password")
                ->nullable()
                ->default(null);
            $table->string("address")
                ->nullable()
                ->default(null);
            $table->string("city")
                ->nullable()
                ->default(null);
            $table->string("state")
                ->nullable()
                ->default(null);
            $table->integer("zipcode")
                ->nullable()
                ->default(null);
            $table->string("email")
                ->nullable()
                ->default(null);
            $table->dateTime("created_at")
                ->nullable()
                ->default(null);
            $table->dateTime("updated_at")
                ->nullable()
                ->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            Schema::dropIfExists("users");
        });
    }

}