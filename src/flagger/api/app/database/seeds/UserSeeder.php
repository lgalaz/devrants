<?php

class UserSeeder extends DatabaseSeeder
{
    public function run()
    {
        $users = [
            [
                "username" => "lgalaz",
                "password" => Hash::make("password"),
                "email"    => "lgalaz@gmail.com"
            ],
            [
                "username" => "amendez",
                "password" => Hash::make("password"),
                "email"    => "lgalaz@gmail.com"
            ]
        ];

        foreach ($users as $user)
        {
            $usr = User::where('username', '=', $user['username'])->first();
            if ($usr==null)
            {
                User::create($user);
            }
        }

    }
}