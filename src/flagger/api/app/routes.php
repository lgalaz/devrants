<?php

Route::pattern('id', '[1-9][0-9]*');
//will only accept dates in YYYY-mm-dd format
Route::pattern('date', '^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(["before" => "guest"], function()
{
    Route::any("/", [
        "as"   => "user/login",
        "uses" => "UserController@login"
    ]);
    Route::any("/signup", [
        "as"   => "user/signup",
        "uses" => "UserController@signup"
    ]);
    Route::any("/request", [
        "as"   => "user/request",
        "uses" => "UserController@request"
    ]);
    Route::any("/reset", [
        "as"   => "user/reset",
        "uses" => "UserController@reset"
    ]);
});

Route::group(["before" => "auth"], function()
{
    Route::any("/profile", [
        "as"   => "user/profile",
        "uses" => "UserController@profile"
    ]);

    Route::any("/logout", [
        "as"   => "user/logout",
        "uses" => "UserController@logout"
    ]);
});



Route::get('api/users/{username}', [
    "as"   => "users/get",
    "uses" => "UserController@get"
]);
