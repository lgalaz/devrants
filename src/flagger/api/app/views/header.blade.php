@section("header")
<div class="header">
    <div class="container">
        <h1>Flagger. Capture all the goddamn flags.</h1>
        @if (Auth::check())
        <a href="{{ URL::route("user/logout") }}">
        logout
        </a>
        |
        <a href="{{ URL::route("user/profile") }}">
        profile
        </a>
        @else
        <a href="{{ URL::route("user/login") }}">
        login
        </a>
        <a href="{{ URL::route("user/signup") }}">
        sign up
        </a>
        @endif
    </div>
</div>
@show