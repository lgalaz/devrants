@extends("layout")
@section("content")
{{ Form::open([
"route"        => "user/signup",
"autocomplete" => "off"
]) }}
<div class="error">
    Sign up
</div>
{{ Form::label("username", "Username") }}
{{ Form::text("username", Input::get("username"), [
"placeholder" => "juan perez"
]) }}
{{ Form::label("password", "Password") }}
{{ Form::password("password", [
"placeholder" => "••••••••••"
]) }}
@if ($error = $errors->first("password"))
<div class="error">
    {{ $error }}
</div>
@endif
{{ Form::submit("register") }}
{{ Form::close() }}
@stop
@section("footer")
@parent
<script src="//polyfill.io"></script>
@stop