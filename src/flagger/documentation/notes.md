API Definition
==============

# /api/user/:id 
	- method:get 
	- response: {name:'', color:''}

# /api/login
	- method:post	
	- postdata:{name:'',hash:''}
	- response: status code: 500/200,{token:''} 

# /api/game/generate
	- method:post
	- postadata: {width:0, height:o, players [,], token}
	- response: status code: 500/200, {data: [,], widht:0, height:0}

# /api/game/actions
	- method:post
	- postdata: {token='', pos:[x,y]}
	- response: status code 500/200, {players:[id,pos,flags], finished:false}	